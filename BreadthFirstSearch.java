class BreadthFirstSearch {
   public static final int INFINITY = Integer.MAX_VALUE;
   private boolean[] marked;
   private int[] edgeTo;
   private int[] distTo;

   public BreadthFirstSearch(Graph g, int s) {
      marked = new boolean[g.V()];
      distTo = new int[g.V()];
      edgeTo = new int[g.V()];
      bfs(g, s);
   }

   private void bfs(Graph g, int s) {
      Queue<Integer> q = new Queue<Integer>();
      for (int v = 0; v < g.V(); v++) {
         distTo[v] = INFINITY;
      }
      distTo[s] = 0;
      marked[s] = true;
      q.enqueue(s);

      while (!q.isEmpty()) {
         int v = q.dequeue();
         for (int w: g.adj(v)) {
            if (!marked[w]) {
               edgeTo[w] = v;
               distTo[w] = distTo[v] + 1;
               marked[w] = true;
               q.enqueue(w);
            }
         }
      }
   }

   public int distTo(int v) {
      return distTo[v];
   }
}

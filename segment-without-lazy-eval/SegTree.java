package rmq;

import java.util.Arrays;

/**
 * Non algs4 implementation This implementation uses plain extra array. Most
 * used i think
 * 
 * This one is simple segment tree without lazy propagation
 * 
 * @author pupusing
 *
 */
public class SegTree {
   private int[] array;
   private int size;

   private int[] heap;
   private Integer[] pendingVal;

   public SegTree(int[] array) {
      this.array = Arrays.copyOf(array, array.length);
      /**
       * max size of storage declare will be 2 * 2 ^ (log2(n) + 1) if we start
       * with 1 indexing
       */
      size = (int) (2 * Math.pow(2.0, Math.floor((Math.log((double) array.length) / Math.log(2.0)) + 1)));

      heap = new int[size];

      /**
       * setting default value since heap stores index, default value is -1
       */
      Arrays.fill(heap, -1);
      pendingVal = new Integer[size];
      build(1, 0, this.array.length - 1);
   }

   public int size() {
      return array.length;
   }

   private void build(int node, int lo, int hi) {
      if (lo == hi) {
         heap[node] = lo; // note that we are storing index, not actual value
      } else {

         // build childs
         int mid = lo + (hi - lo) / 2;
         build(2 * node, lo, mid);
         build(2 * node + 1, mid + 1, hi);

         if (array[heap[2 * node]] <= array[heap[2 * node + 1]]) {
            heap[node] = heap[2 * node];
         } else {
            heap[node] = heap[2 * node + 1];
         }
      }
   }

   public int query(int l, int r) {
      return query(1, 0, array.length - 1, l, r);
   }

   /**
    * search min in range [l,r]
    * 
    * @param node
    * @param start
    *           the start of current node range
    * @param end
    *           if current node range
    * @param l
    * @param r
    * @return
    */
   private int query(int node, int start, int end, int l, int r) {

      /**
       * invalid index returned as -1;
       */
      if (r < start || end < l) {
         return -1;
      }
      if (l <= start && end <= r) {
         return heap[node];
      }

      int mid = start + (end - start) / 2;
      int p1 = query(2 * node, start, mid, l, r);
      int p2 = query(2 * node + 1, mid + 1, end, l, r);

      if (p1 == -1) {
         return heap[node] = p2;
      }

      if (p2 == -1) {
         return heap[node] = p1;
      }

      if (array[p1] < array[p2]) {
         return heap[node] = p1;
      } else {
         return heap[node] = p2;
      }
   }
   

   public static void main(String[] args) {
      int arr[] = { 1, 3, 5, 7, 9, 11 };
      SegTree st = new SegTree(arr);
      System.out.println(st.query(1, 3));
      System.out.println(st.query(3, 5));
      System.out.println(st.query(2, 4));
   }
}

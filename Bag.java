import java.util.Iterator;
import java.util.NoSuchElementException;

public class Bag<E> implements Iterable<E> {
   private Node<E> first;
   private int N;

   private static class Node<E> {
      private E data;
      private Node<E> next;
   }

   public Bag() {
      first = null;
      N = 0;
   }

   public boolean isEmpty() {
      return first == null;
   }

   public int size() {
      return N;
   }

   public void add(E data) {
      Node<E> oldFirst = first;
      first = new Node<E>();
      first.data = data;
      first.next = oldFirst;
      N++;
   }

   public Iterator<E> iterator() {
      return new ListIterator<E>(first);
   }

   private class ListIterator<E> implements Iterator<E> {
      private Node<E> current;
      public ListIterator(Node<E> first) {
         current = first;
      }
      public boolean hasNext() {
         return current != null;
      }
      public void remove() { throw new UnsupportedOperationException(); }
      public E next() {
         if (!hasNext()) throw new NoSuchElementException();
         E data = current.data;
         current = current.next;
         return data;
      }
   }

   public static void main(String[] args) {
      Bag<Integer> bag = new Bag<Integer>();
      bag.add(1);
      bag.add(2);
      bag.add(3);
      for (Integer i: bag) {
         System.out.println(i);
      }
   }
}

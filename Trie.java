import java.util.Scanner;

public class Trie<Value> {
	private static final int R = 256; //extended ASCII

	private Node root;
	private int n; // no. of keys in Trie

	private static class Node {
		private Object val;
		private Node[] next = new Node[R];
	}

	public Trie() {
		// empty constructor
		// as per impl. its ok to initialize root as 0
	}
	
	/**
	 * Get value associated with a key (if key present)
	 * else return null
	 */
	public Value get(String key) {
		Node node = get(root, key, 0);
		if (node == null) return null;
		else return (Value) node.val;
	}
	
	private Node get(Node node, String key, int d) {
		if (node == null) return null;
		if (d == key.length()) {
			return node;
		}
		char c = key.charAt((d));
		return get(node.next[c], key, d + 1);
	}

	public void put(String key, Value val) {
		root = put(root, key, val, 0);
	}

	/**
	 * private helper method for put
	 */
	public Node put(Node node, String key, Value val, int d) {
		if (node == null) {
			node = new Node();
		}
		if (d == key.length()) {
			if (node.val == null) {
				n++;
				// otherwise just reinserting same key again
			}
			node.val = val;
			return node;
		}
		char c = key.charAt(d);
		node.next[c] = put(node.next[c], key, val, d + 1);

		return node;
	}

	public int size() {
		return n;
	}
	
	public boolean isEmpty() {
		return size() == 0;
	}

	/**
	 * removes key from trie if present
	 */
	public void delete(String key) {
		root = delete(root, key, 0);
	}
	private Node delete(Node node, String key, int d) {
		if (node == null) {
			return null;
		}
		if (d == key.length()) {
			if (node.val != null) {
				n--;
			}
			node.val = null; // imp. to do after decrement check
		} else {
		   char c = key.charAt(d);
		   node.next[c] = delete(node.next[c], key, d + 1);
		}
		// if node val is not null, return node immdiately
		if (node.val != null) return node;
		// check suvtrue rooted at node
		for (int i = 0; i < R; i++) {
			if (node.next[i] != null) {
				// if even one subtrie is non empty, return node
				return node;
			}
		}
		// means everything is null including val and all subtrie rooted at node
		// safe to wipe out and return null
		// This cleanup is a very time taking op upto order O(R)
		// hope that we don't need to delete key from trie
		return null;
	}
	
	/**
	 * Return all the strings for which the given string is a prefix
	 */
	
	public Iterable<String> keysWithPrefix(String prefix) {
		Node node = get(root, prefix, 0);
		Queue<String> resultQueue = new Queue<String>();
		
		collect(node, new StringBuilder(prefix), resultQueue);
		
		return resultQueue;
	}
	
	private void collect(Node node, StringBuilder prefix, Queue<String> results) {
		if (node == null) return;
		// node value is set means some string finishes at this point
		if (node.val != null) {
			results.enqueue(prefix.toString());
		}
		
		for (char c = 0; c < R; c++) {
			prefix.append(c);
			collect(node.next[c], prefix, results);
			prefix.deleteCharAt(prefix.length() - 1);
		}
	}
	
	public String longestPrefixOf(String query) {
		int length = longestPrefixOf(root, query, 0, -1);
		if (length == -1) return null;
		else return query.substring(0, length);
	}
	
	/**
	 * private method for returning length of longest match with given string as 
	 * prefix
	 */
	private int longestPrefixOf(Node node, String query, int d, int length) {
		if (node == null) return length;
		if (node.val != null) length = d;
		if (query.length() == d) return length; // reached to end of query length
		char c = query.charAt(d);
		
		return longestPrefixOf(node.next[c], query, d + 1, length);
	}
	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		sc.nextLine();
		
		Trie<Integer> trie = new Trie<Integer>();
		for (int i = 0; i < n; i++) {
			String[] line = sc.nextLine().split("\\s+");
			trie.put(line[0], Integer.parseInt(line[1]));
		}
		
		// check keys with prefix
		for (String key: trie.keysWithPrefix("sh")) {
			System.out.println(key);
		}
		
		System.out.println("Longest prefix of quicksort");
		System.out.println(trie.longestPrefixOf("quicksort"));
		System.out.println();
		
		System.out.println("Longest prefix of shellsort");
		System.out.println(trie.longestPrefixOf("shellsort"));
		
	}
	
	
}

package string;

import java.math.BigInteger;
import java.util.Random;

public class RabinKarp {
   static int R = 256; // radix
   static long mod = longRandomPrime();
   private static int getFirstOccurance(String s, String p) {
      
      int n = s.length();
      int m = p.length();
      
      if (n < m) return -1;
      
      long patHash = hash(p, m);
      long txtHash = hash(s, m); // start hash
      
      if (patHash == txtHash && check(s, p, 0)) return 0;
      
      long RM = 1; // R^(m - 1) % mod for Radix multiplier
      for (int i = 1; i < m; i++) {
         RM = (R * RM) % mod;
      }
      
      for (int i = m; i < n; i++) {
         // removing leading term
         txtHash = (txtHash + mod - RM * s.charAt(i - m) % mod) % mod;
         // multiplying and adding last
         txtHash = (txtHash * R + s.charAt(i)) % mod;
         
         int offset = i - m + 1;
         if (patHash == txtHash && check(s, p, offset)) return offset;
      }
      
      return -1;
   }
   
   
   /**
    * compute hash from [0, m - 1]
    * @param key
    * @return
    */
   private static long hash(String key, int m) {
      long h = 0;
      for (int i = 0; i < m; i++) {
         h = (R * h + key.charAt(i)) % mod;
      }
      return h;
   }
   // check if pattern p match from index i in string s
   public static boolean check(String s, String p, int i) {
      int m = p.length();
      for (int j = 0; j < m; j++) {
         if (s.charAt(i + j) != p.charAt(j)) return false;
      }
      return true;
   }
   
   private static long longRandomPrime() {
      BigInteger prime = BigInteger.probablePrime(31, new Random());
      return prime.longValue();
   }
   
   public static void main(String[] args) {
      // System.out.println(check("geeks for geeks", "geeks", 10));
      System.out.println(getFirstOccurance("THIS IS A TEST TEXT","TEST"));
   }
}

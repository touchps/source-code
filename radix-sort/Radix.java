import java.util.Arrays;

public class Radix {
   /**
    * Algorithm for sorting integer numbers, only +ve
    * 
    * uses lsd version of radix sort for integer sorting 
    * @param a
    */
   public static void lsdSort(int[] a) {
      /**
       *  multiplier used to separate out trailing digits
       *  which are already covered in sorting
       */
      
      int multiplier = 1;
      int n = a.length;
      /**
       * we need to go through digits till max number found so far,
       * which means for others it can be 0
       */
      int max = Integer.MIN_VALUE;
      
      // the auxillary array used at each digit step
      int[] aux = new int[n];
      
      // 10 digits to cover
      int R = 10;
      
      // determining max first
      for (int i = 0; i < n; i++) {
         max = Math.max(max, a[i]);
      }
      
      // while we run out of digits for max number found so far
      while (max / multiplier > 0) {
         /**
          * each loop is a counting sort now for digit
          */
         int[] count = new int[R + 1]; // only 0-9 numbers
         
         // the count loop
         for (int i = 0; i < n; i++) {
            /**
             * a[i] / multiplier actually bring digit under consideration
             * to last position
             * num % 10 then extracts last digit
             */
            int num = a[i] / multiplier;
            count[(num % 10) + 1] += 1;
         }
         
         // cumulative frequency
         for (int i = 1; i <= R; i++) {
            count[i] += count[i - 1];
         }
         
         // copying into auxillary
         for (int i = 0; i < n; i++) {
            int num = a[i] / multiplier;
            aux[count[num % 10]] = a[i];
            count[num % 10] += 1;
         }
         
         // copying back into a
         for (int i = 0; i < n; i++) a[i] = aux[i];
         
         multiplier *= 10;
      }
   }
   
   /**
    * LSD sort for string sorting (assuming small alphabetical characters only)
    * @param a
    */
   public static void lsdSort(String[] a) {
      int n = a.length;
      int max = 0;
      String[] aux = new String[n];
      int R = 27;
      
      // determining max length among all strings
      for (String str: a) max = Math.max(max, str.length());
      
      
      // starting comparison from last index
      for (int indx = max - 1; indx >= 0; indx -= 1) {
         // System.out.println("Comparison fro indx " + indx);
         /**
          * R + 2 required to make room for null characters (for shorter length strings) 
          * it makes sure to rank "aa" smaller than "aaa"
          * 0 is assigned for non existence characters
          */
         int[] count = new int[R + 1];
         
         for (int i = 0; i < n; i++) {
            
            int ch_indx;
            // 0 for non present characters due to shorter string
            if (indx >= a[i].length()) ch_indx = 'a' - 'a';
            else ch_indx = a[i].charAt(indx) - 'a' + 1; // otherwise start from 1
            
            // System.out.println(a[i] + " - > " + ch_indx);
            
            count[ch_indx + 1] += 1; // increment necessary as usual
         }
         
         // usual cumulative count
         for (int i = 1; i <= R; i += 1) {
            count[i] += count[i - 1];
         }
         
         /**
          * same logic to determine index and copy back into auxillary
          */
         for (int i = 0; i < n; i++) {
            int ch_indx;
            if (indx >= a[i].length()) ch_indx = 'a' - 'a';
            else ch_indx = a[i].charAt(indx) - 'a' + 1;
            
            aux[count[ch_indx]] = a[i];
            count[ch_indx] += 1;
         }
         
         
         // copy back to original array
         for (int i = 0; i < n; i++) {
            a[i] = aux[i];
         }
      }
   }
   
   public static void main(String[] args) {
      int[] a = {1201,2912,1291,1293,1203,24};
      String[] str = {"aa","a","ab","bca","abc","aab","bca","bbc","aaaabc","",""};
      
      //lsdSort(a);
      lsdSort(str);
      System.out.println(Arrays.toString(str));
   }
}

/**
 * Return topological order using reverse postorder
 * assuming input graph is DAG
 */
class Topological {
   private Iterable<Integer> order; // topological order
   private int[] rank; // position of vertex in topological sequence

   // assuming graph is DAG
   public Topological(EdgeWeightedDigraph g) {
      DepthFirstOrder dfs = new DepthFirstOrder(g);
      order = dfs.reversePost();
      /** experimental code
      rank = new int[g.V()];
      int i = 0;
      for (int v: order) {
         rank[v] = i++;
      }
      **/
   }

   public Iterable<Integer> order() {
      return order;
   }
}

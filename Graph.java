/**
 * Undirected Graph where vertices range from 0 to V - 1
 */
class Graph {
   private final int V;
   private int E;
   private Bag<Integer>[] adj;

   public Graph(int V) {
      if (V < 0) throw new IllegalArgumentException("Number of vertices must be non negative");
      this.V = V;
      this.E = 0;
      adj = (Bag<Integer>[]) new Bag[V];
      for (int v = 0; v < V; v++) {
         adj[v] = new Bag<Integer>();
      }
   }

   public int V() {
      return V;
   }

   public int E() {
      return E;
   }

   private void validateVertex(int v) {
      if (v < 0 || v >= V) {
         throw new IndexOutOfBoundsException("vertex must be between 0 to V - 1");
      }
   }

   public void addEdge(int v, int w) {
      validateVertex(v);
      validateVertex(w);
      E++;
      adj[v].add(w);
      adj[w].add(v);
   }

   public Iterable<Integer> adj(int v) {
      validateVertex(v);
      return adj[v];
   }

   public int degree(int v) {
      validateVertex(v);
      return adj[v].size();
   }
}

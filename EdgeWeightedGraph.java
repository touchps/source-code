public class EdgeWeightedGraph {
   private final int V;
   private int E;
   private Bag<Edge>[] adj;

   public EdgeWeightedGraph(int V) {
      if (V < 0) throw new IllegalArgumentException();
      this.V = V;
      this.E = 0;
      adj = (Bag<Edge>[]) new Bag[V];
      for (int v = 0; v < V; v++) {
         adj[v] = new Bag<Edge>();
      }
   }

   public int V() {
      return V;
   }

   public int E() {
      return E;
   }

   private void validateVertex(int v) {
      if (v < 0 || v >= V) {
         throw new IndexOutOfBoundsException();
      }
   }

   public void addEdge(Edge e) {
      int v = e.either();
      int w = e.other(v);
      validateVertex(v);
      validateVertex(w);
      adj[v].add(e);
      adj[w].add(e);
      E++;
   }

   public Iterable<Edge> adj(int v) {
      validateVertex(v);
      return adj[v];
   }

   public int degree(int v) {
      validateVertex(v);
      return adj[v].size();
   }


}

public class MathMod {

    private int x;   // Bezout coffecients
    private int y;   // Bezout coffeciants
    private int gcd; // gcd from extended eculidean algo

   /**
    * Method for calculating eculidean gcd
    * of numbers a and b 
    * @param a an integer value
    * @param b another integer value
    * @return gcd of numbers a and b
    * 
    */
   public static int gcd(int a, int b) {
      if (b == 0) {
         return a;
      } else {
         return gcd(b, a % b);
      }
   }

   /**
    * GCD calculation with extended eculidean algo 
    * with side output of Bezout coffecients
    * @param a int
    * @param b int
    */
   public void eculideanGCD(int a, int b) {
      int s0 = 1, s1 = 0;
      int t0 = 0, t1 = 1;
      int r0 = a;
      int r1 = b;

      int temp;

      while (r1 != 0) {
         int q = r0 / r1;
         
         temp = r0;
         r0 = r1;
	 r1 = temp - q * r1;

         temp = s0;
         s0 = s1;
	 s1 = temp - q * s1;

	 temp = t0;
	 t0 = t1;
	 t1 = temp - q * t1;
      }

      x = s0;
      y = t0;
      gcd = r0;
   }

   // Helper method for gcd and bezout coffecients
   public int getGCD() {
      return gcd;
   }

   public int getBezoutX() {
      return x;
   }

   public int getBezoutY() {
      return y;
   }

   /**
    * boolean method to check if extended eculidean can solve 
    * modular multiplicative inverse problem
    */
   public boolean hasModInverse() {
      return gcd == 1;
   }

   /**
    * return modular multiplicative inverse
    * otherwise throws IllegalArgumentException
    * @param a 
    * @param b
    * @return modular multiplicative inverse of a (mod b) as (x+b) if x < 0, x otherwise
    * 
    */
   public int getModInverse(int a, int b) {
      eculideanGCD(a, b);
      if (gcd != 1) {
         throw new IllegalArgumentException("Numbers are not coprime. Extended eculidea");
      }
      if (x < 0) {
         return (x + b);
      } else {
         return x;
      }
   }
   /**
    * static function for modular exponentiation
    * @param base number
    * @param exponent number
    * @param modulus number
    * @return a^b mod c where b can be very large
    *
    */
   public static long modExponent(long base, long exponent, long modulus) {
      if (modulus == 1) return 0;
      // assert (mod - 1) * (mod - 1) do not overflow base (pending implementation)
      long result = 1L;
      base = base % modulus;
      while (exponent > 0) {
         if (exponent % 2 == 1) {
	    result = (result * base) % modulus;
	 }
	 exponent = exponent >> 1;
	 base = ((base % modulus) * (base % modulus)) % modulus; // without inner mod, it can overflow
      }
      return result;
   } 

   /**
    * Calculation binomial coefficiant/combination formula of nCk = !n / (!k * !(n - k))
    */
   public static long combination(int n, int k) {
      long result = 1L;
      // cNk = cN(k-1)
      if (k > n - k) {
         k = n - k;
      }
      for (int i = 0; i < k; i++) {
         result = result * (n - i);
	 result = result / (i + 1);
      }

      return result;
   }

   public static void main(String[] args) {
      //System.out.println(MathMod.gcd(16, 10));

      MathMod m = new MathMod();
      MathMod m1 = new MathMod();
      m.eculideanGCD(17, 43);
      m1.eculideanGCD(11, 3);
      System.out.println("GCD=" + m.getGCD() + " x=" + m.getBezoutX() + " y=" + m.getBezoutY() + " inv=");
      System.out.println("GCD=" + m1.getGCD() + " x=" + m1.getBezoutX() + " y=" + m1.getBezoutY());

      System.out.println(m.getModInverse(17, 43));
      System.out.println("Mod exponentiation: " + modExponent(2, 8291, 100000));
      System.out.println("Mod exponentiation: " + modExponent(2, 7520, 100000));
      System.out.println("Binomial Coefficiant: " + combination(829373,773937));

   }

}

import java.util.Random;

public class Arbi {
   private static Random random;
   private static long seed;

   static {
      seed = System.currentTimeMillis();
      random = new Random(seed);
   }

   private Arbi(){}

   public static void setSeed(long s) {
      seed = s;
      random = new Random(seed);
   }

   public static long getSeed() {
      return seed;
   }

   // random real number
   // [0, 1)
   public static double uniform() {
      return random.nextDouble();
   }

   // random integer
   // [0, n)
   public static int uniform(int n) {
      return random.nextInt(n);
   }

   // uniform integer
   // [a, b)
   public static int uniform(int a, int b) {
      return a + uniform(b - a);
   }

   // uniform double
   // [a, b)
   public static double uniform(double a, double b) {
      return uniform() * (b - a);
   }

   public static void shuffle(int[] a) {
      int n = a.length;
      for (int i = 0; i < n; i++) {
         int r = i + uniform(n - i);
         int temp = a[i];
         a[i] = a[r];
         a[r] = temp;
      }
   }

   // shuffle in range [lo, hi]
   public static void shuffle(int[] a, int lo, int hi) {
      for (int i = lo; i < hi; i++) {
         int r = i + uniform(hi - i);
         int temp = a[i];
         a[i] = a[r];
         a[r] = temp;
      }
   }
}

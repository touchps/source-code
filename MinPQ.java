// Min priority queue
public class MinPQ {
   int[] pq;
   int n;

   public MinPQ(int initCapacity) {
      pq = new int[initCapacity + 1];
      n = 0;
   }

   public void insert (int num) {
      pq[++n] = num;
      swim(n);
   }

   public int peekMin() {
      return pq[1];
   }

   public int size() {
      return n;
   }

   public boolean isEmpty() {
      return n == 0;
   }

   public int extractMin() {
      int num = pq[1];
      pq[1] = pq[n--];
      sink(1);
      return num;
   }

   // swim, after insertion at bottom
   private void swim(int i) {
      while (i > 1 && isGreater(i / 2, i)) {
         swap(i, i / 2);
         i = i / 2;
      }
   }

   // sink bigger element (from root)
   private void sink(int i) {
      // while there is at least one children (2i)
      while (2 * i <= n) {
         int j = 2 * i;
         // we want smaller if both children present
         if (j < n && isGreater(j, j + 1)) j += 1;
         if (!isGreater(i, j)) break;
         swap(i, j);
         i = j;
      }
   }

   // greater than utility
   private boolean isGreater(int i, int j) {
      return pq[i] > pq[j];
   }

   private void swap(int i, int j) {
      int tmp = pq[i];
      pq[i] = pq[j];
      pq[j] = tmp;
   }

   public static void main(String[] args) {
      MinPQ pq = new MinPQ(10);
      pq.insert(10);
      pq.insert(6);
      pq.insert(3);
      pq.insert(1);
      while (!pq.isEmpty()) {
         System.out.println(pq.extractMin());
      }
 
   }
}

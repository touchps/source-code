/**
 * AVL tree
 */
public class BalancedTree<Key extends Comparable<Key>, Value> {
   private Node root;

   private class Node {
      private Key key;
      private Value val;
      private Node left, right;
      private int size; // size of tree rooted at this node
      private int ht; //height

      public Node(Key key, Value val) {
         this.key = key;
         this.val = val;
         this.ht = 1; // height of new node is 0
         this.size = 1;

         this.left = null;
         this.right = null;
      }
   }

   public int height() {
      return height(root);
   }

   /**
    * insert wrapper for no value
    * @param key [description]
    */
   public void insert(Key key) {
      root = insert(root, key);
   }

   /**
    * insert for key only ops
    * @param   [description]
    * @param   [description]
    * @return  [description]
    */
   public Node insert(Node root, Key key) {
      // System.out.println("insert " + key);
      return insert(root, key, null);
   }

   /**
    * BST insertion with balanced tree
    * @param   Node root in which insertion to be performed
    * @param   key
    * @param   value
    * @return  Node root of tree after insertion and re balance
    */
   private Node insert(Node root, Key key, Value val) {
      if (root == null) {
         return new Node(key, val);
      }
      int cmp = key.compareTo(root.key);
      if (cmp < 0) root.left = insert(root.left, key, val);
      else if (cmp > 0) root.right = insert(root.right, key, val);
      else root.val = val; // will not be used often for keys only

      // System.out.println("Set height for " + root.key);
      setHeight(root);
      // System.out.println("new height is " + root.ht);
      setSize(root);

      return resetBalance(root);
   }

   /**
    * Wrapper for deleting key from balanced tree
    * @param key to be deleted 
    */
   private void deleteKey(Key key) {
      root = deleteKey(root, key);
   }

   private Node deleteKey(Node root, Key key) {
      if (root == null) return null;
      int cmp = key.compareTo(root.key);
      if (cmp < 0) root.left = deleteKey(root.left, key);
      else if (cmp > 0) root.right = deleteKey(root.right, key);
      else {
         if (root.right == null) return root.left;
         if (root.left == null) return root.right;

         Node temp = root;
         root = min(temp.right);
         root.right = deleteMin(temp.right);
         root.left = temp.left;
      }
      setSize(root);
      setHeight(root);

      return resetBalance(root);
   }

   /**
    * Get min value from a bst
    * @param  node of the tree in which min value to be searched
    * @return min key
    */
   private Node min(Node node) {
      while (node.left != null) {
         node = node.left;
      }
      return node;
   }

   /**
    * Delete mininum from tree
    * @param  root Node
    * @return Node after deletion
    */

   private Node deleteMin(Node root) {
      if (root.left == null) return root.right;
      root.left = deleteMin(root.left);
      setHeight(root);
      setSize(root);

      return root;
   }

   /**
    * left rotation for a node when balance factor > 1 in AVL tree
    * @param  root node around which rotation is performed
    * @return Node after rotation applied
    */

   private Node rotateLeft(Node node) {
      // System.out.println("left rotation at " + node.key);
      if (node == null) throw new NullPointerException("Null node");

      Node r = node.right;
      node.right = r.left;
      r.left = node;


      // Its important to set size of node first since after rotation it becomes
      // right node of its previous left. Same goes for setting the size below
      setHeight(node);
      setHeight(r);

      setSize(node);
      setSize(r);

      return r;
   }

   /**
    * right rotation for a node when balance factor < -1 in AVL tree
    * @param  root node around which rotation is performed
    * @return Node after rotation applied
    */
   private Node rotateRight(Node node) {
      // System.out.println("Right rotation at " + node.key);
      if (node == null) throw new NullPointerException("Null node");

      Node l = node.left;
      node.left = l.right;
      l.right = node;

      // Its important to set size of node first since after rotation it becomes
      // right node of its previous left. Same goes for setting the size below
      setHeight(node);
      setHeight(l);

      setSize(node);
      setSize(l);

      return l;
   }

   /**
    * Resets the balance of a node by applying rotate operations
    * as required.
    * @param  node
    * @return node
    */
   private Node resetBalance(Node node) {
      int balance = balance(node);
      if (balance > 1) { // left subtree loaded
         if (balance(node.left) < 0) { // left right case
            node.left = rotateLeft(node.left);
         }
         node = rotateRight(node); //left left case
      }

      if (balance < -1) { // right subtree case
         if (balance(node.right) > 0) { // right left case
            node.right = rotateRight(node.right);
         }
         node = rotateLeft(node);
      }
      return node;
   }

   /**
    * Get size of tree rooted at this node
    * @param  Node
    * @return size of the tree
    */
   public int size(Node node) {
      if (node == null) {
         return 0;
      } else {
         return node.size;
      }
   }

   /**
    * reset height of a node
    * @param node Node
    */
   private void setSize(Node node) {
      node.size = size(node.left) + size(node.right) + 1;
   }

   /**
    * get height of the node. Note that height could change
    * during a rotate, insert or delete operation for some nodes
    * - 0 if leaf node
    * - -1 if null node
    * - otherwise height attribute of the node
    * @param  node [description]
    * @return      [description]
    */
   private int height(Node node) {
      if (node == null) return 0;
      return node.ht;
   }

   /**
    * reset height of node as max of height of left/right subtree + 1
    * @param Node
    */
   private void setHeight(Node node) {
      // System.out.println("left: " + height(node.left) + " right: " + height(node.right));
      node.ht = Math.max(height(node.left), height(node.right)) + 1;
   }

   private int balance(Node node) {
      if (root == null) return 0;
      return height(node.left) - height(node.right);
   }

   public void inOrder() {
      inOrder(root);
      System.out.println();
   }

   public void inOrder(Node node) {
      if (node != null) {
         inOrder(node.left);
         System.out.print(node.key + "[" +  node.ht + "][" + node.size + "] ");
         inOrder(node.right);
      }
   }

   public static void main(String[] args) {
      BalancedTree bt = new BalancedTree<Integer, Integer>();

      bt.insert(20);
      // System.out.println(bt.height());
      bt.insert(10);
      // System.out.println(bt.height());
      bt.insert(40);
      // System.out.println(bt.height());
      // bt.insert(15);
      // System.out.println(bt.height());
      bt.insert(7);
      // System.out.println(bt.height());
      bt.inOrder();
      System.out.println(bt.height());
      bt.insert(9);
      System.out.println(bt.height());

      bt.inOrder();
      System.out.println();
   }

}

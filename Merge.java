/**
 * this class provides static method for sorting an array using merge sort
 * as well as counting inversions using merge sort
 * @author touchps
 **/
public class Merge {
   // this class should not be instantiated
   private Merge() {}

   /** stable merge of a[lo .. mid] and a[mid + 1 .. hi] using aux[lo .. hi]
     * as auxillary array.
     * Precondition: a[lo .. mid] and a[mid .. hi] are sorted subarrays
    **/
   private static void merge(Comparable[] a, Comparable[] aux, int lo, int mid, int hi) {
      //copy to auxillary
      for (int k = 0; k <= hi; k++) {
         aux[k] = a[k];
      }

      // merging back to a[]
      int i = lo, j = mid + 1;
      for (int k = 0; k <= hi; k++) {
         if (i > mid) {
            a[k] = aux[j++];
         } else if (j > hi) {
            a[k] = aux[i++];
         } else if (less(aux[j], aux[i])) {
            a[k] = aux[j++];
         } else {
            a[k] = aux[i++];
         }
      }
   }

   



   /**
    * mergesort a[lo .. hi] using auxillary array aux[lo .. hi]
    **/
    private static void sort(Comparable[] a, Comparable[] aux, int lo, int hi) {
       if (hi <= lo) return;
       int mid = lo + (hi - lo) / 2;
       sort(a, aux, lo, mid);
       sort(a, aux, mid + 1, hi);
       merge(a, aux, lo, mid, hi);
    }
   /**
    * helper function for sorting
    **/
    private static boolean less(Comparable v, Comparable w) {
       return v.compareTo(w) < 0;
    }

}

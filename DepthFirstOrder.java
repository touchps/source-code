/**
 * class for calulating depth first order
 * assuming input graph is DAG
 * preordering - order by first visitation
 * postordering - order by last visitation
 * reverse postordering - vertices in opposite order of last visit
 */
class DepthFirstOrder {
   private boolean[] marked;
   private int[] pre;
   private int[] post;
   private Queue<Integer> preorder;
   private Queue<Integer> postorder;
   private int preCounter;
   private int postCounter;

   public DepthFirstOrder(EdgeWeightedDigraph g) {
      pre = new int[g.V()];
      post = new int[g.V()];
      preorder = new Queue<Integer>();
      postorder = new Queue<Integer>();
      marked = new boolean[g.V()];
      for (int v = 0; v < g.V(); v++) {
         if (!marked[v]) {
	    dfs(g, v);
	 } 
      }
   }

   // run dfs in edge weighted digraph from source vertex s
   private void dfs(EdgeWeightedDigraph g, int s) {
      marked[s] = true;
      pre[s] = preCounter++;
      preorder.enqueue(s);
      for (DirectedEdge e: g.adj(s)) {
         int w = e.to();
	 if (!marked[w]) {
	    dfs(g, w);
	 }
      }
      postorder.enqueue(s);
      post[s] = postCounter++;
   }

   public Iterable<Integer> pre() {
      return preorder;
   }

   public Iterable<Integer> post() {
      return postorder;
   }

   public Iterable<Integer> reversePost() {
      Stack<Integer> reverse = new Stack<Integer>();
      for (int v: postorder) {
         reverse.push(v);
      }
      return reverse;
   }
}

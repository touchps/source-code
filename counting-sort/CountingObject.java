package sort;
/**
 * Counting sort for sorting [15-45] students by age, R = 31[0 to 30] (45 - 15)
 * @author touchps
 *
 */
public class CountingObject {
   static int R = 31;
   static int start = 15;
   
   public static void sort(Student[] students) {
      int[] count = new int[R + 1];
      int n = students.length;
      
      Student[] aux = new Student[n];
      for (int i = 0; i < n; i++) {
         int num = students[i].age - start;
         /**
          * This can be a point of mistake, since max value is (45 - 15) = 30(+1 for count index) = 31
          * so if R = 30, spacing of [R + 1] will fail since it will give max value of 30.
          * 
          * Be careful in alotting values, in this it should be R = 31 [0, 30] range
          */
         count[num + 1] += 1;
      }
      
      for (int i = 1; i <= R; i++) {
         count[i] += count[i - 1];
      }
      
      for (int i = 0; i < n; i++) {
         int num = students[i].age - start;
         aux[count[num]] = students[i];
         count[num] += 1;
      }
      
      for (int i = 0; i < n; i++) students[i] = aux[i];
      
   }
   
   public static void main(String[] args) {
      Student[] students = new Student[] {new Student("Sam",16),
                                          new Student("Lisa",20),
                                          new Student("Tom", 15),
                                          new Student("Andre",16),
                                          new Student("Lock",45),
                                          new Student("Nux",24),
                                          new Student("Anna",15),
                                          new Student("Maria",20)};
      
      StringBuilder sb = new StringBuilder();
      sort(students);
      for (Student std: students) {
         sb.append(std + "\n");
      }
      /**
       * output is a stable sort
       */
      System.out.println(sb.toString());
   }
}

// student class with age between [15-45] sized 30
class Student {
   String name;
   int age;
   
   public Student (String name, int age) {
      this.name = name;
      this.age = age;
   }
   
   public String toString() {
      return age + ": " + name;
   }
}

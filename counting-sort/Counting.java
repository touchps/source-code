import java.util.Arrays;
/**
 * Counting sort for
 * - array of char
 * - array of +v1 integers between [0, R - 1]
 * @author pupusing
 *
 */
public class Counting {
   /**
    * counting sort for integers where R determines the range of given numbers [0, R - 1]
    * meaning of Y is the value of biggest number R = Y + 1, or count is size R + 2
    * @param a
    * @param R
    */
   public static void sort(int[] a, int R) {
      int n = a.length;
      int[] aux = new int[n];
      int[] count = new int[R + 1];
      
      for (int i = 0; i < n; i++) {
         count[a[i] + 1] += 1; // +1 very imp. to shift accumulation to be less than next pos
      }
      
      /**
       * this accumulation sets the count strictly less than a given number
       * so count[i] basically fixes the starting position of a number
       */
      for (int r = 1; r <= R; r++) {
         count[r] += count[r - 1];
      }
      
      /**
       * key point of mistake. user of a[i] is imp. piece
       * to use as index in count
       */
      for (int i = 0; i < n; i++) {
         aux[count[a[i]]] = a[i];
         count[a[i]] += 1;
      }
      
      for (int i = 0; i < n; i++) {
         a[i] = aux[i];
      }
   }
   
   /**
    * counting sort for single character alphabet only
    * @param a
    */
   public static void sort(char[] a) {
      int R = 26;
      int n = a.length;
      int[] count = new int[R + 1];
      char[] aux = new char[n];
      
      for (int i = 0; i < n; i++) {
         count[a[i] - 'a' + 1] += 1;
      }
      
      for (int i = 1; i <= R; i++) {
         count[i] += count[i - 1];
      }
      
      
      for (int i = 0; i < n; i++) {
         char c = aux[count[a[i] - 'a']];
         aux[count[a[i] - 'a']] = a[i];
         count[a[i] - 'a'] += 1;
      }
      
      for (int i = 0; i < n; i++) {
         a[i] = aux[i];
      }
   }
   
   public static void main(String[] args) {
      int[] a = {1,1,3,5,7,1,4,7,8,5,2,4,6,9,9,0,7,0};
      char[] b = {'z','c','u','e','a','b','i','a','t','j','l','e','a','g'};
//      sort(a, 10);
      sort(b);
      System.out.println(Arrays.toString(b));
   }
}

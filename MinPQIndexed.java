/**
 * This class allows to change the value of items stored in a PQ.
 * Index value is usually provided with insert which stays fix throughout
 * pq has entry which points to another array qp
 * for keys pq uses same entry as index for keys[]. 
 * During ia exchange, only pq and qp are swapped, key[] remains unaffected
 *
 * @author Puneet Singh
 *
 * @param <Key> generic key type for this priority queue
 * 
 **/
import java.util.NoSuchElementException;


public class MinPQIndexed<Key extends Comparable<Key>> {
   private int maxN;      // max no. of elements on PQ
   private int N;         // no. of elements on PQ
   private int[] pq;      // binary heap with 1-based indexing
   private int[] qp;      // inverse of pq pq[qp[i]] = qp[pq[i]] = i
   private Key[] keys;    // keys[i] = priority of i, never changed
   
   /**
    * Initialize empty indexed priority queue with index between 0 and maxN - 1.
    * @param maxN the keys for indexing on this priority queue from 0 to maxN - 1
    *
    * @throws IllegalArgumentException if maxN &lt; 0
    *
    */
   public MinPQIndexed(int maxN) {
      if (maxN < 0) throw new IllegalArgumentException();
      keys = (Key[]) new Comparable[maxN + 1];
      pq = new int[maxN + 1];
      qp = new int[maxN + 1];
      for (int i = 0; i < maxN; i++) {
         qp[i] = -1;
      }
   
   }

   /**
    * Returns true if priority queue is empty.
    * @return true if priority queue is empty;
    *         false otherwise
    */

   public boolean isEmpty() {
      return N == 0;
   }

   /**
    * Is i an index on this priority queue?
    * @param i an index
    * @return true if i in an index on priority queue
    *         false otherwise
    * @throws IndexOutOfBoundsException unless 0 &le; i &lt; maxN
    */
   public boolean contains(int i) {
      if (i < 0 || i >= maxN) throw new IndexOutOfBoundsException();
      return qp[i] != -1;
   }

   /**
    * Returns the number of keys on this priority queue
    *
    * @return the number of keys on this priority queue
    */
   public int size() {
      return N;
   }

   /**
    * Returns index associated with mininum key
    * @return an index associated with min. key
    * @throws NoSuchElementException if priority queue is empty
    */
   public int minIndex() {
      if (N == 0) throw new NoSuchElementException("PQ underflow");
      return pq[1];
   }

   /**
    * Returns the minimum key
    * @return a minimum key
    * @throws NoSuchElementException if priority queue is empty
    */
   public Key minKey() {
      if (N == 0) throw new NoSuchElementException("PQ underflow");
      return keys[pq[1]];
   }

   /**
    * Removes minimum key and return its associated index, useful for
    * returning vertex index with minimum distance in prim's algo
    * @return index associated with minimum key
    * @throws NoSuchElementException if priority queue is empty
    */
   public int delMin() {
      if (N == 0) throw new NoSuchElementException("PQ underflow");
      int min = pq[1];
      exch(1, N--);
      sink(1);
      qp[min] = -1;         // delete
      keys[pq[N+1]] = null; //garbage collection

      return min;
   }

   /**
    * Change the key associated with index i to the specified value.
    * @param i the index of key to be changed. graph vertex in case of prim
    * @param key change the key associated with index i to this key
    * @throws IndexOutOfBoundsException unless 0 &le; i &lt; maxN
    * @throws NoSuchElementException when no key associated with index i
    */

   public void changeKey(int i, Key key) {
      if (i < 0 || i >= maxN) throw new IndexOutOfBoundsException();
      if (!contains(i)) throw new NoSuchElementException("Index not in PQ");
      keys[i] = key;
      swim(qp[i]);
      sink(qp[i]);
   }

   /**
    * Increase key associated with index to a specified value
    * @param  i the index of the key to increase
    * @param  key increase the key assocated with index i to this key
    * @throws IndexOutOfBoundsException unless 0 &le; i &lt; maxN
    * @throws IllegalArgumentException if key &le; key associated with index i
    * @throws NoSuchElementException no key is associated with index i
    */
   public void increaseKey(int i, Key key) {
      if (i < 0 || i >= maxN) throw new IndexOutOfBoundsException();
      if (!contains(i)) throw new NoSuchElementException("index is not in the PQ");
      if (keys[i].compareTo(key) >= 0) {
         throw new IllegalArgumentException("wrong argument for increase");
      }
      keys[i] = key;
      sink(qp[i]);    
   }

   /**
    * Decrease the key associated with index i to the specified value.
    * @param  i the index of the key to decrease
    * @param  key decrease the key assocated with index i to this key
    * @throws IndexOutOfBoundsException unless 0 &le; i &lt; maxN
    * @throws IllegalArgumentException if key &ge; key associated with index i
    * @throws NoSuchElementException no key is associated with index i
    */

   public void decreaseKey(int i, Key key) {
      if (i < 0 || i >= maxN) throw new IndexOutOfBoundsException();
      if (!contains(i)) throw new NoSuchElementException("index is not in the PQ");
      if (keys[i].compareTo(key) <= 0) {
         throw new IllegalArgumentException("wrong argument for decrease");
      }
      keys[i] = key;
      swim(qp[i]);
   }


   /**
    * Helper function for comparison of keys in this priority queue
    */
   private boolean greater(int i, int j) {
      return (keys[pq[i]].compareTo(keys[pq[j]])) > 0;
   }

   /**
    * Heap helper functions
    */
   private void swim(int k) {
      while (k > 1 && greater(k/2, k)) {
         exch(k/2, k);
         k = k/2;
      }
   }

   private void exch(int i, int j) {
      int swap = pq[i];
      pq[i] = pq[j];
      pq[j] = swap;
      qp[pq[i]] = i;
      qp[pq[j]] = j;
   }

   private void sink(int i) {
      while (2*i <= N) {
         int j = 2*i;
         if (j < N && greater(j, j+1)) {
            j++;
         }
         if (!greater(i, j)) {
            break;
         }
      }
   }


}

public class FenwickTree {
   int[] array; // 1-indexed array
   int n;

   public FenwickTree(int size) {
      array = new int[size + 1];
      n = size;
   }

   /**
    * update the index with given value (add)
    */
   public void update(int index, int value) {
      while(index <= n) {
         array[index] += value;

	 index += index & -index;
      }
   }

   public int prefixSum(int index) {
      int sum = 0;
      while(index > 0) {
         sum += array[index];
	 index -= index & (-index);
      }

      return sum;
   }

   /**
    * inclusive range query for sum
    */
   public int rangeQuery(int a, int b) {
      return prefixSum(b) - prefixSum(a - 1);
   }

   public static void main(String[] args) {
      int a[] = {2, 1, 1, 3, 2, 3, 4, 5, 6, 7, 8, 9};
      FenwickTree ft = new FenwickTree(a.length);
      for(int i = 1; i <= a.length; i++) {
         ft.update(i, a[i - 1]);
      }

      System.out.println(ft.rangeQuery(2,4));
      ft.update(2,1);
      System.out.println(ft.rangeQuery(2,4));
   }
}

/**
 * Segment Tree: stores intervals, and optimized for "which of these intervals contains a given point" queries
 * Interval Tree: stores intervals as well, but optimized for "which of these intervals overlap with a given interval" queries 
 * It can also be used for point queries - similar to segment tree
 *
 * Sometimes Segment tree also referred as straightway Interval tree with following queries
 * - which of these intervals contain a given point
 * - which of these points are in a given interval
 */
public class SegmentTree {
   class Node {
      private int start;
      private int end;

      /**
       * data for range query
       */
      private int val; // the data we look for range query

      private Node left, right;

      public Node(int start, int end, int val) {
         this.start = start;
	 this.end = end;
	 this.val = val;

	 this.left = this.right = null;
      }

      public Node(int start, int end) {
         this.start = start;
	 this.end = end;

	 this.left = this.right = null;
      }

      public int start() {
         return this.start;
      }

      public int end() {
         return this.end;
      }

      public int val() {
         return this.val;
      }

      public String toString() {
         StringBuilder sb = new StringBuilder();
	 sb.append("(" + start + "," + end + ")[" + val + "]");
	 return sb.toString();
      }

   }

   /**
    * instance variables of Segment Tree
    */
   private Node root;
   private int[] data;

   public SegmentTree(int start, int end, int[] items) {
      data = items;
      root = build(start, end);
   }

   /**
    * method for building the tree
    */
   private Node build(int start, int end) {
      //System.out.println("building from " + start + " to " + end);
      if (start > end) {
         return null;
      }

      Node node = new Node(start, end);
      if (start == end) {
         node.val = data[start];
         return node;
      }

      int m = start + (end - start) / 2;

      node.left = build(start, m);
      node.right = build(m + 1, end);

      node.val = node.left == null ? 0 : node.left.val;

      node.val = node.right == null ? node.val : Math.max(node.left.val, node.right.val);

      return node;
   }

   private int query(Node node, int start, int end) {
      //System.out.println("call to " + start + " " + end);
      if (node == null) {
         return Integer.MIN_VALUE;
      }


      if (node.start >= start && node.end <= end) {
         return node.val;
      }

      int m = node.start + (node.end - node.start) / 2;
      if (m < start) {
         return query(node.right, start, end);
      } else if (m >= end) {
         return query(node.left, start, end);
      } else {
         return Math.max(query(node.left, start, m), query(node.right, m + 1, end));
      }
   }

   public int query(int start, int end) {
      return query(root, start, end);
   }

   public String toString() {
      StringBuilder sb = new StringBuilder();
      getInorder(sb, root);
      return sb.toString();
   }

   private void getInorder(StringBuilder sb, Node node) {
      if (node == null) return;
      sb.append(node);
      getInorder(sb, node.left);
      getInorder(sb, node.right);
   }

   public static void main(String[] args) {
      int[] a = {1,4,2,3};
      SegmentTree tree = new SegmentTree(0, a.length - 1, a);

      System.out.println(tree.query(1,1));
      System.out.println(tree.query(1,2));
      System.out.println(tree.query(2,3));
      System.out.println(tree.query(0,2));
   }
}

import java.util.Iterator;
import java.util.NoSuchElementException;

class Queue<E> implements Iterable<E> {

   private Node<E> first;
   private Node<E> last;
   private int N;
   private static class Node<E> {
      private E data;
      private Node<E> next;
   }

   public Queue() {
      first = null;
      last = null;
      N = 0;
   }

   public boolean isEmpty() {
      return first == null;
   }

   public int size() {
      return N;
   }

   public void enqueue(E data) {
      Node<E> oldLast = last;
      last = new Node<E>();
      last.data = data;
      last.next = null;
      if (isEmpty()) first = last;
      else oldLast.next = last;
      N++;
   }

   public E dequeue() {
      if (isEmpty()) throw new NoSuchElementException("Queue Underflow");
      E data = first.data;
      first = first.next;
      N--;
      if(isEmpty()) last = null;
      return data;
   }

   public Iterator<E> iterator() {
      return new ListIterator<E>(first);
   }

   public class ListIterator<E> implements Iterator<E> {
      private Node<E> current;
      public ListIterator(Node<E> first) {
         current = first;
      }

      public boolean hasNext() {
         return current != null;
      }

      public void remove() { throw new UnsupportedOperationException(); }

      public E next() {
         if (!hasNext()) throw new NoSuchElementException();
         E data = current.data;
         current = current.next;
         return data;
      }
   }
}

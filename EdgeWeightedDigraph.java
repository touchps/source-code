public class EdgeWeightedDigraph {
   private final int V;
   private int E;
   private Bag<DirectedEdge>[] adj;
   private Bag<DirectedEdge>[] inverseAdj; // for all vertices incoming

   private int[] indegree; // indegree of vertex v

   public EdgeWeightedDigraph(int V) {
      if (V < 0) throw new IllegalArgumentException("Number of vertices in digraph must be non negative");
      this.V = V;
      this.E = 0;
      this.indegree = new int[V];
      adj = (Bag<DirectedEdge>[]) new Bag[V];
      inverseAdj = (Bag<DirectedEdge>[]) new Bag[V];

      for (int v = 0; v < V; v++) {
         adj[v] = new Bag<DirectedEdge>();
         inverseAdj[v] = new Bag<DirectedEdge>();
      }
   }

   /**
    * Initialize a new edge weighted di graph that is deep copy of g
    */
   public EdgeWeightedDigraph(EdgeWeightedDigraph G) {
      this(G.V());
      this.E = G.E();
      for (int v = 0; v < G.V(); v++) {
         this.indegree[v] = G.indegree(v);
      }
   }

   public int V() {
      return V;
   }

   public int E() {
      return E;
   }

   public void addEdge(DirectedEdge e) {
      int v = e.from();
      int w = e.to();
      validateVertex(v);
      validateVertex(w);
      
      adj[v].add(e);
      inverseAdj[w].add(e);
      indegree[w]++;
      E++;
   }

   /**
    * return directed edges incident from vertex
    */

   public Iterable<DirectedEdge> adj(int v) {
      validateVertex(v);
      return adj[v]; // returning bag which is Iterable
   }

   /**
    * return directed edges incident into a vertex
    */

   public Iterable<DirectedEdge> inverseAdj(int v) {
      validateVertex(v);
      return inverseAdj[v]; // returning bag which is Iterable
   }
   /**
    * return number of directed incident edges
    */
   public int indegree(int v) {
      validateVertex(v);
      return indegree[v];
   }

   /**
    * number of directed edge incident from vertex
    */
   public int outdegree(int v) {
      validateVertex(v);
      return adj[v].size();
   }

   /**
    * return all directed edges in the edge weighted directed graph
    */
   public Iterable<DirectedEdge> edges() {
      Bag<DirectedEdge> list = new Bag<DirectedEdge>();
      for (int v = 0; v < V; v++) {
         for (DirectedEdge e: adj(v)) {
	    list.add(e);
	 }
      }
      return list;
   }

   /**
    * throw index out of bounds exceptin unless {@code0 <= v < V}
    */

   private void validateVertex(int v) {
      if (v < 0 || v >= V) {
         throw new IndexOutOfBoundsException("vertex " + v + " is not between 0 and " + (V - 1));
      }
   }


}

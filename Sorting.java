// Integer based sorting
public class Sorting {

   // normal quicksort
   public static void quicksort(int[] a, int low, int high) {
      if (low >= high) return;
      int j = partition(a, low, high);
      quicksort(a, low, j - 1);
      quicksort(a, j + 1, high);
   }

   // lo and hi and range inclusive
   private static int partition(int[] a, int lo, int hi) {
      int i = lo;
      int j = hi + 1;
      int v = a[lo];

      while (true) {
         while(a[++i] < v) {
            if (i == hi) break;
         }

         while(v < a[--j]) {
            if (j == lo) break;
         }

         if (i >= j) break;
      }

      int tmp = a[lo];
      a[lo] = a[j];
      a[j] = tmp;

      return j;
   }

   // 3-way quicksort for [lo, hi]
   public static void quicksortThreeWay(int[] a, int lo, int hi) {
      if (lo >= hi) return;
      int lt = lo;
      int gt = hi;

      int v = a[lo];
      int i = lo + 1;

      while (i <= gt) {
         if (a[i] < v) exch(a, lt++, i++);
         else if (a[i] > v) exch(a, i, gt--);
         else i++;
      }

      quicksortThreeWay(a, lo, lt - 1);
      quicksortThreeWay(a, gt + 1, hi);
   }

   public static void exch(int[] a, int i, int j) {
      int tmp = a[i];
      a[i] = a[j];
      a[j] = tmp;
   }

   public static void mergesort(int[] a, int[] aux, int lo, int hi) {
      if (lo >= hi) return;
      int mid = lo + (hi - lo) / 2;
      mergesort(a, aux, lo, mid);
      mergesort(a, aux, mid + 1, hi);

      merge(a, aux, lo, mid, hi);
   }

   public static void merge(int[] a, int[] aux, int lo, int mid, int hi) {
      for (int i = lo; i <= hi; i++) aux[i] = a[i];

      int i = lo;
      int j = mid + 1;

      for (int k = lo; k <= hi; k++) {
         if (i > mid) a[k] = aux[j++];
         else if (j > hi) a[k] = aux[i++];
         else if (aux[i] < aux[j]) a[k] = aux[i++];
         else a[k] = aux[j++];
      }
   }
}

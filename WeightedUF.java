public class WeightedUF {
   private int[] size;
   private int[] root;
   private int N;

   public WeightedUF(int N) {
      this.N = N;
      size = new int[N];
      root = new int[N];

      for (int i = 0; i < N; i++) {
         size[i] = 1;
	 root[i] = i;
      }
   }

   /**
    * return root node of a node
    */
   public int find(int n) {
      while (root[n] != n) {
         n = root[n];
      }
      return n;
   }

   public void union(int a, int b) {
      int ra = find(a);
      int rb = find(b);

      if (ra != rb) {
         if (size[ra] > size[rb]) {
	    root[rb] = ra;
	    size[ra] += size[rb];
	 } else {
	    root[ra] = rb;
	    size[rb] += size[ra];
	 }
      }
   }

   public boolean connected(int a, int b) {
      return find(a) == find(b);
   }

   public static void main (String[] args) {
      WeightedUF uf = new WeightedUF(10);
      System.out.println(uf.connected(0,1));
      uf.union(1,6);
      uf.union(2,3);
      uf.union(2,1);

   }
}

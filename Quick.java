import java.util.*;
public class Quick {
   /**
    * class shouldn't be instantiated
    */
   public static void main(String[] args) {
      Integer[] a = new Integer[] {2,3,5,1,4};
      Integer[] b = new Integer[] {3,5,1,4};

      //System.out.println(select(a, a.length / 2));
      System.out.println(select(b, b.length / 2));
   }
   private Quick() { }

   public static Comparable select(Comparable[] a, int k) {
      if (k < 0 || k >= a.length) {
         throw new IndexOutOfBoundsException("Selected element out of bounds");
      }

      int lo = 0;
      int hi = a.length - 1;
      while(hi > lo) {
         int i = partition(a, lo, hi);
	 if (i > k) hi = i - 1;
	 else if (i < k) lo = i + 1;
	 else return a[i];
      }

      return a[lo];

   }

   /**
    * partition a[lo..hi] so that a[lo..j-1] <= a[j+1..hi]
    * and return index j
    */
   private static int partition(Comparable[] a, int lo, int hi) {
      int i = lo;
      int j = hi + 1;

      Comparable v = a[lo]; // comparison element

      while(true) {
         // find item on lo side to swap
	 while(less(a[++i], v)) {
	    if (i == hi) break;
	 }

	 while(less(v, a[--j])) {
	    if (j == lo) break;
	 }

	 if (i >= j) break;

	 exch(a, i, j);
      }

      // put partitioning item in its sorted place
      exch(a, lo, j);

      return j;
   }

   /*********************************************
    * Helper functions for sorting and parition
    */
   private static boolean less(Comparable v, Comparable w) {
      return v.compareTo(w) < 0;
   }

   private static void exch(Object[] a, int i, int j) {
      Object swap = a[i];
      a[i] = a[j];
      a[j] = swap;
   }
}

import java.util.Random;

/**
 * class for generating random data for stress testing
 */

public class Stress {
   private static Random random;    // pseudo-random number generator
   private static long seed;        // pseudo-random number generator seed 

   // static initializer
   static {
      seed = System.currentTimeMillis();
      random = new Random(seed);
   }

   /**
    * Sets seed and allows same sequence of random nos to 
    * be generated again
    */
   public static void setSeed(long s) {
      seed = s;
      random = new Random(seed);
   }

   public static long getSeed() {
      return seed;
   }

   /**
    * random real number between [0, 1)
    */
   public static double uniform() {
      return random.nextDouble();
   }

   /**
    * random int between [0, n)
    */
   public static int uniform(int n) {
      if (n <= 0) throw new IllegalArgumentException("parameter n must be +ve");
      return random.nextInt(n);
   }

   /**
    * returns random between[a, b)
    */
   public static int uniform(int a, int b) {
      if (b <= a) throw new IllegalArgumentException("Invalid range");
      if ((long) b - a >= Integer.MAX_VALUE) throw new IllegalArgumentException("Invalid range");
      return a + uniform(b - a);
   }

   /**
    * Returns a random real number uniformly in [a, b).]
    */
   public static double uniform(double a, double b) {
      if (!(a < b)) throw new IllegalArgumentException("Invalid range");
      return a + uniform() * (b-a);
   }

   /**
    * ------------------------------------------------
    *  Methods for generating test data
    */

   /**
    * return a array of n elements with given range 
    * and repetition of numbers allowed
    */
   public static int[] uniformRange(int a, int b, int n) {
      int[] nums = new int[n];

      for (int i = 0; i < n; i++) {
         nums[i] = uniform(a, b);
      }
      return nums;
   }
}

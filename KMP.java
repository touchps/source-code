
import java.util.ArrayList;
import java.util.Arrays;
/*
 * KMP String matching algorithm
 */
   
public class KMP {
   
   private String pattern;
   private int[] lps;
   public KMP(String pattern) {
      this.pattern = pattern;   
      this.lps = prefixArray(pattern);
   }
   
   /**
    * get matching indexes for a given text
    * @param text
    * @return
    */
   public int[] getIndexes(String text) {
      ArrayList<Integer> index = new ArrayList<Integer>();
      int i = 0; // text itr
      int j = 0; // pattern itr
      while(i < text.length()) {
         
         if (pattern.charAt(j) == text.charAt(i)) {
            j += 1;
            i += 1;
         }
         
         if (j == pattern.length()) {
            // pattern found at i - j in text string
            index.add(i - j);
            // fall back to just previous valid prefix index
            j = lps[j - 1];
         } else if (i < text.length() 
               && pattern.charAt(j) != text.charAt(i)) {
            if (j != 0) { // fall back further for prefix match (linear)
               j = lps[j - 1];
            } else {
               i += 1; // start comparison from next text character
            }
         }
      }
      
      return index.stream().mapToInt(num -> num).toArray();
   }
   
   /**
    * get number of matches of pattern in given text
    * @param text
    * @return
    */
   public int getMatchCount(String text) {
      return getIndexes(text).length;
   }
   
   /**
    * This function returns lps arrays which for each index contains
    * longest proper prefix which also a suffix of string ending at i index
    * @param pattern
    * @return
    */
   private static int[] prefixArray(String pattern) {
      int[] lps = new int[pattern.length()];
      
      // length of previous longest prefix
      int tracker = 0;
      
      lps[0] = 0; // starting is always same
      
      int i = 1;
      while(i < pattern.length()) {
         // Note that tracker is for prefix
         // while i represents suffix end match
         if (pattern.charAt(tracker) == pattern.charAt(i)) {
            tracker += 1;
            lps[i] = tracker;
            i += 1;
         } else { // character mismatch
            /**
             * if tracker is not at beginning, 0 length prefix
             * it will fall to previous prefix and check again with same i
             * in next loop
             */
            if (tracker != 0) {
               tracker = lps[tracker - 1];
            } else { // tracker already at beginning, we restart matching from 0
               lps[i] = 0;
               i += 1;
            }
         }
      }
      
      return lps;
   }
   public static void main(String[] args) {
      String pattern = "GEEK";
      KMP kmp = new KMP(pattern);
      System.out.println(Arrays.toString(kmp.getIndexes("GEEKS FOR GEEKS")));
   }
   
}

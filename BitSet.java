public class BitSet {
   private static short[] precomputedParity;
   private static long[] precomputedReverse;

   static final int NUM_UNSIGN_BITS = 63;

   /**
    *  Returns lowesti/rightmost bit as isolated
    */
   public static int lowestIsolated(int x) {
      return x & ~(x - 1);
      // can also use x & (-x)
      // means -x is basically ~(x - 1)
   }

   /**
    * replaces lowest 1 bit with 0
    * Means turns off rightmost 1 bit
    */
   public static int unsetLowest(int x) {
      return x & (x - 1);
   }

   /**
    * Check if a number is power of 2, with special Long type in java
    * representing usigned bit 2^64 - 1
    */

   /**
    * count set bits in a number
    */
   public static int countSetBits(int x) {
      int count = 0, y;
      while (x != 0) {
	 y = lowestIsolated(x);
         x = x ^ y;
	 count++;
      }
      return count;
   }

   /**
    * check if num is even using bit manipulation
    */
   public static boolean isEven(int x) {
      return (x & 1) == 0;
   }

   /**
    * test if nth bit is set
    */
   public static boolean isSet(int x, int n) {
      return (x & (1 << n)) != 0;
   }

   /**
    * Set x with nth bit set
    */
   public static int setBit(int x, int n) {
      return x | (1 << n);
   }

   /**
    * Unset the nth bit in x
    */
   public static int unsetBit(int x, int n) {
      return x & ~(1 << n);
   }

   /**
    * Toggle nth bit in x (uses XOR)
    */

   public static int toggleBit(int x, int n) {
      return x ^ (1 << n);
   }

   /**
    * Right propagate the rightmost 1-bit, setting all bits after it to 1
    */
   public static int propagateBit(int x) {
      return x | (x - 1);
   }

   /**
    * Isolate rightmost 0 and return it as 1 with all others set to 0
    */
   public static int lowestUnset(int x) {
      return ~x & (x - 1);
   }

   /**
    * Set rightmost unset bit
    */
   public static int setLowestUnset(int x) {
      return x | (x + 1);
   }

   /**
    * x mod n where n is power of 2
    */

   public static int powerMod(int x, int n) {
      return x & (n - 1);
   }

   /**
    * get parity odd/even in O(k) time, where k is number of set bits
    * odd - 1
    * even - 0
    */

   public static short getParity(long x) {
      short result = 0;
      while (x != 0) {
         result ^= 1; // keeps toggling result. becomes 0 on full cycle toggle
	 x &= (x - 1); // drops rightmost set bit
      }

      return result;
   }

   static {
      // maxis xFFFF, and to accomodate, declared 1 value large, since indexing in 0 based
      precomputedParity = new short[1 << 16]; 
      for (int i = 0; i < (1 << 16); ++i) {
         precomputedParity[i] = getParity(i);
      }
   }

   /**
    * computing parity of 2^64 bits long numbers using pieces of 
    * precomputed parity of size 2^16
    */
   public static short getParity_1(long x) {
      final int WORD_SIZE = 16;
      final int BIT_MASK = 0xFFFF;

      return (short) (
         (int)precomputedParity[(int) (x >>> (3 * WORD_SIZE)) & BIT_MASK]
         ^ (int)precomputedParity[(int) (x >>> (2 * WORD_SIZE)) & BIT_MASK]
         ^ (int)precomputedParity[(int) (x >>> (1 * WORD_SIZE)) & BIT_MASK]
         ^ (int)precomputedParity[(int) (x  & BIT_MASK)]
      
      );
   }

   /**
    * CPU'S word level xor based parity checker
    */
   public static short cpuParity(long x) {
      x ^= x >>> 32;
      x ^= x >>> 16;
      x ^= x >>> 8;
      x ^= x >>> 4;
      x ^= x >>> 2;
      x ^= x >>> 1;
      return (short) (x & 0x1);
   }

   /**
    * swap bits in a number x between i and j index
    */
   public static long swapBits(long x, long i, long j) {
      // no swapping required if same bits
      if (((x >> i) & 1) != ((x >> j) & 1)) {
         x ^= (1L << i) | (1L << j);
      }
      return x;
   }

   /**
    * linear O(n) reverse of bits with end of bits as parameter
    */
   private static long reverseBits(long x, int n) {
      for (int i = 0, j = n; i < j; i++, j--) {
         x = swapBits(x, i, j);
      }
      return x;
   }

   /**
    * Static block for precomputed reverse initializer
    */
   static {
      precomputedReverse = new long[1 << 16];
      for (int i = 0; i < (1 << 16); i++) {
         precomputedReverse[i] = reverseBits(i, 15);
      }
   }

   /**
    * reverse 64 bit with pre reverse computation for 16 bits
    */
   public static long reverseBits(long x) {
      final int WORD_SIZE = 16;
      final int BIT_MASK = 0xFFFF;
      return precomputedReverse[(int) ((x >>> (3 * WORD_SIZE)) & BIT_MASK)] // first 16 bit shifted to most right, then bit masked
	      | precomputedReverse[(int) ((x >>> (2 * WORD_SIZE)) & BIT_MASK)] << WORD_SIZE // 2nd 16 bit shifted to most right, bit masked and shifted left to 3rd pos
	      | precomputedReverse[(int) ((x >>> WORD_SIZE) & BIT_MASK)] << (2 * WORD_SIZE) // 3rd 16 bit shifted to most right, bit masked and shifted left to 2nd pos
	      | precomputedReverse[(int) (x & BIT_MASK)] << (3 * WORD_SIZE); // 4th/last 16 bit, bit masked and shifted left to 1st pos
   }

   /**
    * Given weight of a number as number of set bits
    * and given a number x, find a number y with same weight x 
    * and |y-x| is minimum
    */
   public static long closestNumberSameWeight(long x) {
      for (int i = 0; i < NUM_UNSIGN_BITS; i++) {
         if (((x >>> i) & 1) !=  ((x >>> (i + 1)) & 1)) {
	    x ^= (1L << i) | (1L << (i + 1)); 
	    return x;
	 }
      }
      throw new IllegalArgumentException("All bits are 0 or 1");
   }

   public static long multiply(long x, long y) {
      long bit = 1L;
      long result = 0L;

      while (x != 0) {
         if ((x & bit) > 0) {
	    result += bit * y; // also need to be implemented without +
	    bit = bit << 1;
	 }
	 x = x >>> 1;
      }

      return result;
   }



   public static void main(String[] args) {
      System.out.println("Count bits " + countSetBits(63));
      System.out.println("Is bit set " + isSet(5, 1));
      System.out.println("Power Mode " + powerMod(51, 32));
      System.out.println("Parity O(k) " + getParity(920307));
      System.out.println("Precomp Parity " + getParity_1(920307));
      System.out.println("Precomp Parity " + getParity_1(920305));
      System.out.println("CPU Parity " + getParity_1(920307992));
      System.out.println("Swap bits " + swapBits(54, 0, 3));
      System.out.println("reverse bits brute " + reverseBits(1, 63));
      System.out.println("reverse bits 16 bit lookup " + reverseBits(-9223372036854775808L));
      System.out.println("Find elements with same weight " + closestNumberSameWeight(7));
      System.out.println("Multiplication of two numbers " + multiply(2,7));
   }

}
